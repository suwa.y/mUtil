# Utilities for ML.

This module provides,
- Logger

# Modules
## mutil.logger.Logger

```python
from mutil.logger.Logger

# This will create log directory named same as script file.
mlg = Logger(__file__)

# Log will added to the log directory.
with mlg() as writer:
    writer("Messages")

# This returns log directory path.
mlg.save_path()

# This returns file path which is in the log directory.
mlg.save_path("path")
```

## Command line arguments.

This module provides command line arguments.

- -n [name]   : Save directory name.
- -c          : Create new save directory. The name is specified by argument "Logger.__init__(save_dir) + number."
- -ln [name]  : Logfile name.
- -nm [number]: You can choose the number of save directory.
- -cp [A]     : Performs copy specified file into save_dir.
- -df [A] [B] : Saves diffs between file A nd B into save_dir.

#### Example
```shell
python any_script.py -n log_directory_name
```

