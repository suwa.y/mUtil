import os
import shutil
import inspect
import contextlib
import datetime
import argparse
import subprocess
from logging import getLogger, StreamHandler, DEBUG, FileHandler


class Logger(object):

    def __init__(self, save_dir, logname="log", create_new=False):
        """
        Arguments can be specified by command line arguments.

        Args:
            save_dir(str): 
            logname(str):
            create_new(bool):

        Option arguments:
            -n [name]   : Save directory name.
            -c          : Create new save directory. The name is specified by argument "Logger.__init__(save_dir) + number."
            -ln [name]  : Logfile name.
            -nm [number]: You can choose the number of save directory.
            -cp [A]     : Performs copy specified file into save_dir.
            -df [A] [B] : Saves diffs between file A nd B into save_dir.
        """
        count = 0
        newly_created = False
        save_dir_base = os.path.splitext(save_dir)[0]

        # Argparse settings
        parser = argparse.ArgumentParser()
        parser.add_argument('-n', '--name', default=save_dir_base,
                            help="Directory to which log files will saved.")
        parser.add_argument('-ln', '--logname', default=logname, help="Logfile name.")
        parser.add_argument('-c', '--create', action='store_const', const=True,
                            default=create_new, help="If this is set, new save directory will be created.")
        parser.add_argument('-nm', '--number', default=count, help="Logfile name.")
        parser.add_argument('-cp', '--copy', nargs=1, help="Logfile name.")
        parser.add_argument('-df', '--diff', nargs=2, help="Logfile name.")

        # args = parser.parse_args()
        args, unknown = parser.parse_known_args()
        save_dir_base = args.name
        create_new = args.create
        logname = args.logname
        number = args.number

        count = int(number)
        self.save_dir = save_dir_base + "_%02d" % (count)
        if not os.path.exists(self.save_dir):
            os.makedirs(self.save_dir)
            newly_created = True
        elif create_new:
            while True:
                count += 1
                new_dir = os.path.join(save_dir_base + "_%02d" % (count))
                if not os.path.exists(new_dir):
                    os.makedirs(new_dir)
                    self.save_dir = new_dir
                    newly_created = True
                    break

        self.logger = getLogger(__name__)
        logfile_path = os.path.join(self.save_dir, "%s.log" % (logname))
        handler = FileHandler(logfile_path)
        handler.setLevel(DEBUG)
        self.logger.setLevel(DEBUG)
        self.logger.addHandler(handler)
        self.logger.propagate = False
        self.logger.debug("Logging started on {}.".format(datetime.datetime.today()))

        # Call commands.
        copied_file = False
        write_diff = False
        if getattr(args, "copy"):
            copied_file = self.copy_file(args.copy[0])
        if getattr(args, "diff"):
            self.write_diff(*args.diff)
            write_diff = "{}, {}".format(*args.diff)

        print()
        print("#### Logging settings")
        print("- save dir:      %s" % (self.save_dir))
        print("- logfile:       %s" % (logfile_path))
        print("- newly created: {}".format(newly_created))
        print("- copied file:   {}".format(copied_file))
        print("- write diff:    {}".format(write_diff))
        print()

    def copy_file(self, filepath):
        filename = "copyof_%s" % os.path.split(filepath)[-1]
        shutil.copyfile(filepath, os.path.join(self.save_dir, filename))
        return os.path.join(self.save_dir, filename)

    def write_diff(self, fA, fB):
        cmd = "diff {} {}".format(fA, fB)
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout_data, stderr_data = p.communicate()
        with open(self.save_path("differences.txt"), "a") as writer:
            message = "Difference between {} and {}.".format(fA, fB)
            writer.write("{}\n".format(message))
            writer.write(stdout_data.decode("utf-8"))
            writer.write("\n")

    @contextlib.contextmanager
    def __call__(self, withdate=True, withprint=False):
        yield self.get_writer(withdate=withdate, withprint=withprint)

    def get_writer(self, withdate, withprint):
        def writer(message, withdate=withdate, withprint=withprint):
            if withprint:
                print(message)
            if withdate:
                s = "{} {}".format(datetime.datetime.today(), message)
            else:
                s = "{}".format(message)
            self.logger.debug(s)
        return writer

    def save_path(self, filename=""):
        return os.path.join(self.save_dir, filename)

    def write_description(self, message):
        with open(self.save_path("description.txt"), "a") as writer:
            writer.write("{}\n".format(message))
